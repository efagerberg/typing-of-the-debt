﻿using UnityEngine;
using System.Collections.Generic;

public class Enemy : MonoBehaviour {
    public static List<Enemy> Enemies = new List<Enemy>();
    public static GameObject Player;
    public bool RandomColor = false;
    private string _text;
    public string Text
    {
        get { return _text; }
        set
        {
            _text = value;
            displayText.text = value;
        }
    }
    public float EnemySpeed = 5.0f;
    private TextMesh displayText;

    // Use this for initialization
    protected void Start()
    {
        Init();
    }

    private bool _initialized = false;
    public void Init()
    {
        if (_initialized) return;
        _initialized = true;
        Player = GameObject.Find("Player");
        displayText = GetComponentInChildren<TextMesh>();
        Enemies.Add(this);

        if (RandomColor)
        {
            GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
        }
        int difficulty = Random.Range(0, 100);
        Text = difficulty < 50 ? WordBank.GetEasyWord() : difficulty < 80 ? WordBank.GetMediumWord() : WordBank.GetHardWord();
    }

    // Update is called once per frame
    void Update () {

        if (Player != null)
        { 
            transform.LookAt(Player.transform);
            transform.position += transform.forward * EnemySpeed * Time.deltaTime;
            displayText.transform.LookAt(Player.transform);
            transform.Rotate(transform.up, 180);
        }
    }

    public bool Check(string inText, bool formattingOnly = false)
    {
        bool result = (Text.Equals(inText, System.StringComparison.InvariantCultureIgnoreCase));
        if (result && !formattingOnly)
        {
            Enemies.Remove(this);
        }
        if (inText != null && Text.StartsWith(inText, System.StringComparison.InvariantCultureIgnoreCase) )
        {
            matchFormatting(inText.Length);
        }
        return result;
    }

    private void matchFormatting(int chars)
    {
        if (chars == 0)
            displayText.text = Text;
        displayText.text = "<color=yellow>" + Text.Substring(0, chars) + "</color>" + Text.Substring(chars);
    }

    public Vector3 GetWordsPosition()
    {
        return displayText.transform.position;
    }

    public void GetKilled()
    {
        if (Enemies.Contains(this))
            Enemies.Remove(this);
        Destroy(gameObject);
    }
}
