﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Swarmer : Enemy
{
    public static Dictionary<string, int> PrefixableWords;
    private int spawns = 3;
    // Use this for initialization
    new void Start()
    {
        base.Start();
        GameObject enemyStandardPrefab = Resources.Load("Prefabs/Enemy") as GameObject;
        for (int i = 0; i < spawns; i++)
        {
            float angle = Random.Range(-Mathf.PI * 2.0f, Mathf.PI * 2.0f);
            Vector3 offset = new Vector3(Mathf.Sin(angle) * 3.0f, 0, Mathf.Cos(angle) * 3.0f);
            Enemy nme = (Instantiate(enemyStandardPrefab, transform.position + offset, transform.rotation) as GameObject).GetComponent<Enemy>();
            nme.Init();
            nme.Text = Text.Substring(0, 4);
        }
    }
}
