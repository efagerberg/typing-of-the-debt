﻿using UnityEngine;

public class EnemyZone : MonoBehaviour
{
    private EnemyTypes types;
    public bool bStandard, bSwarmer, bFast;
    [Range(0, 10000)]
    public float SpawnRate = 100.0f;
    private float last = 0;
    private BoxCollider zone;
    private GameObject enemyStandardPrefab, enemySwarmerPrefab, enemyFastPrefab;
    // Use this for initialization
    void Start()
    {
        zone = GetComponent<BoxCollider>();
        enemyStandardPrefab = Resources.Load("Prefabs/Enemy") as GameObject;
        enemySwarmerPrefab = Resources.Load("Prefabs/Swarmer") as GameObject;
        enemyFastPrefab = Resources.Load("Prefabs/Fast") as GameObject;

        SpawnRate /= 1000.0f;
        types = 0;
        types = types | (EnemyTypes)(bStandard ? 1 : 0);
        types = types | (EnemyTypes)(bSwarmer ? 2 : 0);
        types = types | (EnemyTypes)(bFast ? 4 : 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > last + SpawnRate)
        {
            string word = GetWord();
            if ((types & EnemyTypes.Swarmer) == EnemyTypes.Swarmer)
            {
                //if (Swarmer.PrefixableWords.ContainsKey(word))
                {
                    Swarmer s = (Instantiate(enemySwarmerPrefab, GetPosition(), transform.rotation) as GameObject).GetComponent<Swarmer>();
                    s.Init();
                    s.Text = word;
                }
            }
            else if ((types & EnemyTypes.Fast) == EnemyTypes.Fast)
            {
                //Do Fast Things
            }
            else
            {
                Instantiate(enemyStandardPrefab, GetPosition(), transform.rotation);
            }
            last = Time.time;
        }
    }

    string GetWord()
    {
        return WordBank.GetMediumWord();
    }

    Vector3 GetPosition()
    {
        return transform.position +
            new Vector3(
                zone.size.x * Random.Range(-0.5f, 0.5f),
                zone.size.y * Random.Range(-0.5f, 0.5f),
                zone.size.z * Random.Range(-0.5f, 0.5f)
                );
    }
}

[System.Flags]
public enum EnemyTypes
{
    Standard = (1 << 0),
    Swarmer = (1 << 1),
    Fast = (1 << 2)
}