﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class WordBank
{

    const int EASY_LENGTH_LIMIT = 6;
    const int MEDIUM_LENGTH_LIMIT = 10;

    static HashSet<string> easyWords = new HashSet<string>();
    static HashSet<string> medWords = new HashSet<string>();
    static HashSet<string> hardWords = new HashSet<string>();

    static WordBank()
    {
        string[] files = { "Assets/Resources/random_words.txt" };
        IEnumerable<string> words;

        foreach (string file in files)
        {
            string contents = System.IO.File.ReadAllText(file);
            if (contents != null)
            {
                words = contents.Split('\n').Select(word => word.Trim());
                foreach (string word in words)
                {
                    if (word.Length <= EASY_LENGTH_LIMIT)
                    {
                        if (!easyWords.Contains(word))
                            easyWords.Add(word);
                    }
                    else if (word.Length <= MEDIUM_LENGTH_LIMIT)
                    {
                        if (!medWords.Contains(word))
                            medWords.Add(word);
                    }
                    else if (!hardWords.Contains(word))
                    {
                        hardWords.Add(word);
                    }
                }
            }
        }
    }

    public static string GetEasyWord()
    {
        return easyWords.ElementAt(Random.Range(0, easyWords.Count));
    }

    public static string GetMediumWord()
    {
        return medWords.ElementAt(Random.Range(0, medWords.Count));
    }

    public static string GetHardWord()
    {
        return hardWords.ElementAt(Random.Range(0, hardWords.Count));
    }
}
